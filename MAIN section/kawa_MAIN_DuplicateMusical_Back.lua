﻿--[[ 
* ReaScript Name: kawa_MAIN_DuplicateMusical_Back. 
* Version: 2017/06/29 
* Author: kawa_ 
* link: https://bitbucket.org/kawaCat/reascript-m2bpack/ 
--]] 
local l=0;local s=reaper.CountSelectedMediaItems(l);local o=reaper.GetProjectLength(l);local e=41383 local e=40698 local e=40058 local e=41613 local e=40635 local e=40297 local f=40289 local e=40914 local M=41613 local T="kawa MAIN Duplicate Musical BackWards"local function p(e)local a=reaper.CountSelectedMediaItems(e);local l=reaper.GetProjectLength(e);local i={}local l={}local n=0;while(n<a)do local a=reaper.GetSelectedMediaItem(e,n);local t=reaper.GetMediaItemTrack(a);local e=reaper.GetMediaTrackInfo_Value(t,"IP_TRACKNUMBER");if(l[e]==nil)then l[e]={}local a=reaper.CountTrackMediaItems(t);local r=0;while(r<a)do local a=reaper.GetTrackMediaItem(t,r)local a={mediaItem=a,startTime=reaper.GetMediaItemInfo_Value(a,"D_POSITION"),length=reaper.GetMediaItemInfo_Value(a,"D_LENGTH"),endTime=reaper.GetMediaItemInfo_Value(a,"D_POSITION")+reaper.GetMediaItemInfo_Value(a,"D_LENGTH"),mediaItemIdx=reaper.GetMediaItemInfo_Value(a,"IP_ITEMNUMBER"),trackId=e,mediaTrack=t};table.insert(l[e],a);r=r+1;end table.sort(l[e],function(a,e)return(a.startTime<e.startTime);end);end local a={mediaItem=a,startTime=reaper.GetMediaItemInfo_Value(a,"D_POSITION"),length=reaper.GetMediaItemInfo_Value(a,"D_LENGTH"),endTime=reaper.GetMediaItemInfo_Value(a,"D_POSITION")+reaper.GetMediaItemInfo_Value(a,"D_LENGTH"),mediaItemIdx=reaper.GetMediaItemInfo_Value(a,"IP_ITEMNUMBER"),trackId=e,targetTime=nil,mediaTrack=t};local t=0;for l,e in ipairs(l[e])do if(e.mediaItemIdx==a.mediaItemIdx)then a.targetTime=t;end t=e.endTime end if(i[e]==nil)then i[e]={}end table.insert(i[e],a);n=n+1;end return l,i end local function I(a,l,t)local e=reaper.TimeMap2_timeToQN(a,l);local t=reaper.TimeMap2_timeToQN(a,t);local e=t-e-1e-6;local t,l,a=reaper.TimeMap_GetTimeSigAtTime(a,l+.001)local a=0;local l=t*(4/l)if(e<=.25)then a=.25-e elseif(e<=.5)then a=.5-e elseif(e<=1)then a=1-e elseif(e<=2)then a=2-e elseif(e<=l)then a=l-e else local t=math.floor(e/l);local l=(t+1)*l;a=l-e;end local a=e+a;return a,e end local function m(e)local r=nil;local i=nil;local a=nil;local c=nil;local l=nil;local d=nil;local n=0;for t,e in pairs(e)do local t=o;local t=nil local o=nil for m,e in ipairs(e)do if(r==nil)then r=e.mediaTrack i=e.trackId else if(i>e.trackId)then r=e.mediaTrack i=e.trackId end end if(l==nil)then l=e.startTime;d=e;else if(l>e.startTime)then l=e.startTime;d=e;end end if(a==nil)then a=e.endTime;c=e;else if(a<e.endTime)then a=e.endTime;c=e;end end if(t==nil)then t=e.endTime;else if(t<e.endTime)then t=e.endTime;end end o=math.min(o or e.endTime,e.targetTime)end n=math.max(n,o)end return l,a,r,d,c,n end if(s>0)then reaper.Undo_BeginBlock();local a,e=p(l)local a local a local t,a,r,r,r,r=m(e)local a=I(l,t,a);reaper.Main_OnCommandEx(f,0,l)for t,e in pairs(e)do for t,e in ipairs(e)do local t=e.startTime-reaper.TimeMap2_QNToTime(l,a)if(e.startTime>0)then reaper.PreventUIRefresh(100)local a=reaper.AddMediaItemToTrack(e.mediaTrack)local r,e=reaper.GetItemStateChunk(e.mediaItem,"",false)local e=reaper.SetItemStateChunk(a,e,false);local e=reaper.SetMediaItemPosition(a,math.max(t,0),false)reaper.SetMediaItemSelected(a,true)reaper.Main_OnCommandEx(M,0,l)reaper.PreventUIRefresh(-1)end end end reaper.Undo_EndBlock(T,-1);reaper.UpdateArrange();end
