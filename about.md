# ReaScript-MIDI for ReaPack.

* ReaScript For Cockos REAPER Midi Piano Roll Editor.
* Scripts info and Animation Preview at [Wiki Page](http://kawa.works/reascript-midi-section). 
* Some script need ["SWS / S&M Extension"](http://www.sws-extension.org/).
* Thank you Reaper And ReaPack And Sws extensiton!

# Animation Preview and Documents.

1.  [MIDI-Section](http://kawa.works/reascript-midi-section)
2.  [MIDI-CC-Section](http://kawa.works/reascript-midi-cc-section)
3.  [ENVELOPE-Section](http://kawa.works/reascript-envelope-section)
4.  [MAIN-Section](http://kawa.works/reascript-main-section)
5.  [GUI-Section](http://kawa.works/reascript-gui-section)
6.  [Change log](http://kawa.works/reascript-change-log)
7.  [Labo](https://bitbucket.org/kawaCat/reascript-m2bpack/wiki/Laboratory)
