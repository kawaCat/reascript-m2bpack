
@rem bat file for Sourcetree's gitbush.
@rem ".\\_reBuildGit.bat" command on sourcetree git bush.
@rem =========================================================
SET TEMP_DIR="_temporary"
@rem =========================================================
@rem move new Files to git ignore directory
mkdir %TEMP_DIR%
move "ENVELOPE section" %TEMP_DIR%/"ENVELOPE section"
move "GUI section" %TEMP_DIR%/"GUI section"
move "JSFX effects" %TEMP_DIR%/"JSFX effects"
move "MAIN section" %TEMP_DIR%/"MAIN section"
move "MIDI editor" %TEMP_DIR%/"MIDI editor"

@rem =========================================================
@rem commit change(deleting files on git new commit)
git add -u 
git commit -m "--"
@rem =========================================================
@rem rebase first commit ID
git rebase -i 8ea93e3
@rem =========================================================
@rem cleaning files(in git)
git reflog expire --expire=now --all
git gc --aggressive --prune=now 
@rem =========================================================

@rem restore script files.
@rem =========================================================
move %TEMP_DIR%/"ENVELOPE section" "ENVELOPE section" 
move %TEMP_DIR%/"GUI section" "GUI section" 
move %TEMP_DIR%/"JSFX effects" "JSFX effects" 
move %TEMP_DIR%/"MAIN section" "MAIN section" 
move %TEMP_DIR%/"MIDI editor" "MIDI editor" 
@rem =========================================================
@rem apply new change and commit to git
git add -A
git add -u 
git commit -m "--"
@rem =========================================================

@rem start cmd with other process. and create reapck index
@rem =========================================================
start Reapack_Create.bat  
@rem =========================================================

@rem git push -f
